using Microsoft.AspNetCore.Mvc;
using ProfileB.Models;

namespace ProfileB.Controllers
{
    public class BusinessController : Controller
    {
        public IActionResult Index()
        {
            var user = new User()
            {
                Id = 1,
                Name = "Hotch",
                Email = "hotchlin@gmail.com",
                Password = "hotch"
            };

            return View(user);
        }
        
        public IActionResult GetUser()
        {
            var user = new User()
            {
                Id = 1,
                Name = "Hotch",
                Email = "hotchlin@gmail.com",
                Password = "hotch"
            };
            
            return Json(user);
        }
  
        public IActionResult Error()
        {
            return View();
        }
    }
}